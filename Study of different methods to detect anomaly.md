# AC Motor Anomaly Detection

These are the major outside factors that are reasons for the motor breakdown.


- **Voltage Imbalance**:If there exist any voltage imbalance to the motor, then the motor draws excessive current from one or more phases so as to satisfy the torque. The excessive current causes to increase the temperature which makes the motor to breakdown.In this can we can use either voltage sensor or current sensor, because voltage imbalance causes excessive current or Excessive current tells us there is voltage imbalance.

        Components: Voltage Sensor can be used to measure the Voltage.
                    Current Sensor can be used to measure the Current.

- **Temperature**:High Temperature inside the motor results in Insulating failure. For every 10oc that the temperature of a motor rises , the insulation life reduced by 50%. As we can't check the temperature inside the motor we here by check the temperature of operating environment. The temperature of operating environment varies by -30oc compared to the temperature inside the motor.

        Components:Temperature Sensor can be used to measure the Temperature.

- **Operational overloads**:Motor overload occurs when a motor is under excessice load. Excessive current draw is one of the symptoms to say that motor is having heavy load

        Components: Current Sensor can be used to measure the Current.

- **Harmonic distortion**:Harmonics are the unwanted high frequency AC voltages or currents . This additional sources causes internal energy losses  which dissipate in the form of heat, which destroy the insulation capability of the windings. We no need to check this conditon as it is already included before, but it helps while we  display why the motor beakdown occured.
      
      Components: Voltage and Current sensor can be used.


- **Insulation Error**: In motor, Insulation has both electrical resistance and capacitance. If insulation is perfect then there will be little leakage current around both paths. But if the insulation is old or damaged, the resistance is lower so there will be high leakage of current along higher capacitance also cause large current leakage.

        Components: Current Sensor can be used.














