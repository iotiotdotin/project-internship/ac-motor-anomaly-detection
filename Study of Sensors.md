# Study of Sensors

The Sensors that are required for Anomaly Dectection in an AC Motor are:


- Current Sensors

- Voltage Sensors

- Temperature Sensors


## Current Sensors

### 1.ACS712

![current sensor](extras/Current.png)

- The Current Sensors we use is ACS712 Current Sensors which can be easily used with micro controllers like the
Arduino, NodeMCU etc.,

- This sensor is based on the Allegro ACS712ELC chip.

- This sensors is available in three different modules, they are: 5A, 20A and 30A Modules.

- Current Sensor detects the current in a wire or conductor and generates a signal proportional to the detected current either in the form of analog voltage or digital output

- This IC can detect both AC and DC current .

![current modules](extras/current1.png)

#### ACS712 Module Pin Outs

![pin diagram](extras/current2.png)

#### Connections

![connection](extras/current3.png)


[Datasheet-ACS712](https://www.rhydolabz.com/documents/28/ACS712_Current_Sensor_Datasheet_Buy_India.pdf)



### 2.SCT013000

![Current sensor](extras/sct013000.jpg)

- Current transformers (CTs) are sensors that are used for measuring alternating current. 
- This is a non-invasive current sensor which is clamped around the supply line to measure a load upto 100 Amps.
- The cable is terminated on one end with a standard 3.5mm jack.
- The split core type in the CT can be clipped onto either the live or neutral wire where we get the power source.

![specifications](extras/sct013000-specifications.png)

#### Connections:

**Power Source Connections:**

![sensorinstallation](extras/sct013000connection.jpg)

**ESP32 connections:**

![Connections](extras/zmpt101b-esp32.png)


[SCT-013-000 Datasheet](http://statics3.seeedstudio.com/assets/file/bazaar/product/101990029-SCT-013-000-Datasheet.pdf)



## Voltgae Sensor

![voltage sensor](extras/ZMPT101B.png)

- ZMPT101B voltage sensor module is a voltage sensor made from the ZMPT101B voltage transformer. This is frequently called as AC Voltage Sensor.
- It has high accuracy, good consistency for voltage and power measurement.
- It can measure up to 250V AC.
-  It is simple to use and comes with a multi turn trim potentiometer for adjusting the ADC output.

![specification](extras/zmptspecifications.png)

### Pin Description

![Sensor pins](extras/ZMPT101Bpin.png)


### Connections

![Connections](extras/zmpt101b-esp32.png)


[ZMPT101B-Datasheet](https://innovatorsguru.com/wp-content/uploads/2019/02/ZMPT101B.pdf)

# Temperature Sensor

## 1.DS18B20


![Sensor](extras/DS18B20.jpg)


- The DS18B20 temperature sensor is a one-wire digital temperature sensor.
-  This only requires one data line (and GND) to communicate with your ESP32.
- It can be powered in two ways, They are: 1. Parasite Mode  2. Normal Mode.

        - Normal Mode is nothing but giving external power supply.

        - In Parasite Mode the power is driven from the data line.

- It can measure temperatures from -55°C to +125°C with ±0.5°C Accuracy.

![specifications](extras/ds18b20specifications.png)

### Pins Of DS18B20

![pinconfiguration](extras/ds18b20pins.png)

### Connections Of DS18B20 with ESP32

**Normal Mode:**

![mode1](extras/normalmode.png)

**Parasite Mode:**

![mode2](extras/parasitemode.png)

[Datasheet-DS18B20](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)


### 1.K-Type Thermocouple with MAX6675

![thermocouple](extras/k-type.png)

- K-Type Thermocouple is general-purpose thermocouple, mostly commonly used to measure temperature.
- A thermocouple produces a temperature-dependent voltage due to the thermoelectric effect.
- The challenge with using thermocouples is with the need for what is known as cold junction compensation.


**Cold junction compensation:**

        - The Cold junction compensation compensates for the missing thermoelectric voltage due to thermocouple

        - It satisfies the need to detect a very small voltage change for every degree in a change of temperature.
        
        - This allows electronics to use the thermoelectric voltage tables to determine the temperature.


**MAX6675**

![sensor module](extras/MAX6675.jpeg)

- Fortunately,there are chips like the MAX6675 to fulfill this need.
- The device measures the output of a K Thermocouple and provides the result to the Arduino or MCU via an SPI interface.
- The MAX6675 measures the voltage from the thermocouple's output and gives us the digital temperature output.

**MAX6675 specifications:**

![specifications](extras/specifications-max6675.png)

**Pin outs:**

![pins](extras/MAX6675-Thermocouple-TemperatureConnections.png)

**Connections:**

![MCU connections](extras/Sensor-ThermoCouple-MCU.jpg)


[Datasheet of MAX6675](https://www.rhydolabz.com/documents/32/MAX6675.pdf)

For more details of thermocouple visit:[K-type thermocouple](https://www.tegam.com/what-exactly-is-cold-junction-compensation/)





